import json
from datetime import datetime
from statistics import mean, median


def merge_data(json_data, users, items):
    """Inserts data to users and items data structures from given JSON object.
    users: set of users_id
    items: dictionary in format: {item_id1: [(time1, variant1), (time2, variant2) ...], item_id2: ...}"""

    for key in json_data.keys():
        users.add(key)

    for (person_id, item_ids_dict) in json_data.items():
        item_ids_dict.pop("variant")

        for item_id, item_value in item_ids_dict.items():

            for item_data in item_value[0]:
                request_time, variant = datetime.fromisoformat(item_data[0]), item_data[1]
                if item_id in items:
                    items[item_id] += [(request_time, variant)]
                else:
                    items[item_id] = [(request_time, variant)]


def load_data(files, users, items):
    """Loads two data structures useful for given tasks."""
    try:
        for file in files:
            with open(file) as f:
                merge_data(json.load(f), users, items)
            print("File loaded: " + file)
        return True
    except FileNotFoundError:
        print("File not found.")
    except:
        print("File exists, but something went wrong. Probably wrong data format.")
    return False


def calculate_result(users, items):
    """Calculates necessary information (given task) and returns them as a tuple."""

    result = [0 for _ in range(5)]

    # Task 1
    result[0] = len(users)
    print("Task 1 completed.")

    # Task 2
    result[1] = len(items)
    print("Task 2 completed.")

    # Task 3, 4 --- result in seconds
    all_time_deltas = []
    for item_data_list in items.values():
        # all time differences between requests of all users related to certain item
        item_data_list.sort(key=lambda x: x[0])
        all_time_deltas += [item_data_list[i + 1][0] - item_data_list[i][0]
                            for i in range(len(item_data_list) - 1)]

    result[2] = mean(map(lambda x: x.total_seconds(), all_time_deltas))
    print("Task 3 completed.")
    result[3] = median(map(lambda x: x.total_seconds(), all_time_deltas))
    print("Task 4 completed.")

    # Task 5
    result[4] = max(
        [len([i for i in x if i[1] == "similarInJsonList"]) for x in items.values()]
    )
    print("Task 4 completed.")

    return tuple(result)


if __name__ == "__main__":

    _files = [f'data/{str(i).zfill(2)}_returned' for i in range(24)]
    users_set, items_dict = set(), {}
    if load_data(_files, users_set, items_dict):
        print(calculate_result(users_set, items_dict))
